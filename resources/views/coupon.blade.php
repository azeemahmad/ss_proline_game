
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proline</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet prefetch" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/app.css">
</head>
<body class="bodyouter">
<header class="logo-header">
        <img src="img/logo.png" class="aligh-left" alt="logo"> <img src="img/logo-lifestyle.png" class="aligh-right" alt="logo">
    </header>
<div class="container form-outer">
    <!-- <header>
        <img src="img/logo.png">
    </header> -->
     @if(isset($alradayplayed) && $alradayplayed==1)
        <h1>Congrats!</h1>
        <h3>But you have already played.</h3>
           @elseif($coupunid=='XXXXXXX')
        <h1>Congrats!</h1>
        <h3 class="text-center">Thank you for playing<br>You have't got any coupon <br>because your played time more than 1 minute or no more coupons left.</h3>      
         @else
    <h1>Congrats!</h1>
    <h3>CouponCode: {{$coupunid}}</h3>
    <h3 class="text-center">Thank you for playing. <br>You have received <br>a voucher of <b>Rs. {{$discout}}</b>.<br> This voucher can be used at the LifeStyle Store on purchase of Proline apparels. Voucher valid till 23rd Oct 2019. To view T&C, check  <a href="{{url('/termandconditions')}}">click here</a></h3>
         @endif

</div>


<a class="button tncbtn" href="{{url('/termandconditions')}}">Terms & Conditions</a>

<div id="popup2" class="overlay">
    <div class="popup">
        <h2>Terms & Conditions</h2>
        <a class="close" href="#">&times;</a>
        <div class="content">
            <ul class="tnclist">
                <li>1. You will receive a voucher based on your performance in your first game only. </li>
                <li>2. 30 seconds and below - Voucher worth Rs. 1000 redeemable on shopping worth Rs. 1999.</li>
                <li>3.  60 seconds  to 30 seconds - Vouchers worth 500 redeemable on shopping worth 1499.</li>
                <li>4. 60 seconds and above - Vouchers worth Rs.250 redeemable on shopping worth 999.</li>
                <li>5. Offer valid from 31st August to 15th September, 2019. </li>
                <li>6. Offer Valid only at the Proline Store - Sarath City mall Hyd.   Gachibowli - Miyapur Road, White Field Rd, Kondapur, Hyderabad, Telangana 500084. </li>
          </ul>
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="js/app.js"></script>
<script>
    $(document).ready(function() {
        var host_url = location.protocol + "//" + location.host;
        setTimeout(function() {
            window.location.replace(host_url+'/logout');
        }, 5000);
    });
</script>
</body>
</html>
