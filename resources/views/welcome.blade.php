<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proline</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <link rel="stylesheet prefetch" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link rel="stylesheet" href="css/app.css">

    <style type="text/css">
    
</style>
</head>
<body>

    <header class="logo-header">
        <img src="img/logo.png" class="aligh-left" alt="logo"> <img src="img/logo-lifestyle.png" class="aligh-right" alt="logo">
    </header>

<div class="container">
    <!-- <header>
        <img src="img/logo.png">
    </header> -->
     <h3>Match all the pairs as quickly as possible! </h3>

    <section class="score-panel">
        <ul class="stars" style="display: none;">
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
        </ul>
        <span>Level 1 | </span>
        <span class="moves">0</span> Move(s)

        <div class="timer">
        </div>

        <div class="restart" onclick=startGame()>
            <i class="fa fa-repeat"></i>
        </div>
    </section>

    <ul class="deck" id="card-deck">
        <li class="card" type="1">
            <img src="img/level1/1.jpg">
        </li>
        <li class="card" type="2">
            <img src="img/level1/2.jpg" alt="">
        </li>
        <li class="card match" type="3">
            <img src="img/level1/3.jpg" alt="">
        </li>
        <li class="card" type="4" >
            <img src="img/level1/4.jpg" alt="">
        </li>
        <li class="card" type="5">
            <img src="img/level1/5.jpg" alt="">
        </li>
        <li class="card match" type="3">
            <img src="img/level1/3.jpg" alt="">
        </li>
        <li class="card" type="6">
            <img src="img/level1/6.jpg" alt="">
        </li>
        <li class="card" type="7">
            <img src="img/level1/7.jpg" alt="">
        </li>
        <li class="card" type="1">
            <img src="img/level1/1.jpg" alt="">
        </li>
        <li class="card" type="8">
            <img src="img/level1/8.jpg" alt="">
        </li>
        <li class="card" type="6">
            <img src="img/level1/6.jpg" alt="">
        </li>
        <li class="card" type="8">
            <img src="img/level1/8.jpg" alt="">
        </li>
        <li class="card open show" type="4">
            <img src="img/level1/4.jpg" alt="">
        </li>
        <li class="card" type="7">
            <img src="img/level1/7.jpg" alt="">
        </li>
        <li class="card" type="2">
            <img src="img/level1/2.jpg" alt="">
        </li>
        <li class="card" type="5">
            <img src="img/level1/5.jpg" alt="">
        </li>
    </ul>

    <div id="popup1" class="overlay">
        <div class="popup">
            <h2>Congratulations</h2>
            <a class="close" href=# >×</a>
            <div class="content-1">
                Congratulations you're a winner
            </div>
            <div class="content-2">
                <p>You made <span id="finalMove"> </span> moves </p>
                <p>in <span id="totalTime"> </span> </p>
                <p style="display: none;">Rating:  <span id="starRating"></span></p>
            </div>
            <button id="play-again"onclick="playAgain()">
                Play again 😄</a>
            </button>
        </div>
    </div>

</div>


<a class="button tncbtn" href="{{url('/termandconditions')}}">Terms & Conditions</a>

<div id="popup2" class="overlay">
    <div class="popup">
        <div class="popup-tnc">
            <h2>Terms & Conditions</h2>
            <a class="close" href="#">&times;</a>

            <div class="content">
                <ul class="tnclist">
                    <li>1. Voucher is valid at our Exclusive Proline Stores and <a href="https://www.prolineindia.com">Prolineindia.com</a></li>
                    <li>2. Vouchers are based on your time taken to finish the game.</li>
                    <li>3. Vouchers codes are sent to you via SMS/Email.</li>
                    <li>4. This voucher cannot be clubbed with any other offer.</li>
                    <li>5. Only one voucher can be redeemed at a time.</li>
                    <li> 6. For any questions please call our customer care number for details.</li>
                    {{--<li>1. You will receive a voucher based on your performance in your first game only. </li>--}}
                    {{--<li>2. 30 seconds and below - Voucher worth Rs. 1000 redeemable on shopping worth Rs. 1999.</li>--}}
                    {{--<li>3.  60 seconds  to 30 seconds - Vouchers worth 500 redeemable on shopping worth 1499.</li>--}}
                    {{--<li>4. 60 seconds and above - Vouchers worth Rs.250 redeemable on shopping worth 999.</li>--}}
                    {{--<li>5. Offer valid from 31st August to 15th September, 2019. </li>--}}
                    {{--<li>6. Offer Valid only at the Proline Store - Sarath City mall Hyd.   Gachibowli - Miyapur Road, White Field Rd, Kondapur, Hyderabad, Telangana 500084. </li>--}}
                </ul>

                <table style="width: 550px;" border="0" cellspacing="0" cellpadding="0">
                    <colgroup>
                        <col width="201"/>
                        <col width="229"/>
                        <col width="116"/>
                        <col width="98"/>
                    </colgroup>
                    <tbody>
                    <tr style="height: 20px;">
                        <td><strong>Time to complete</strong></td>
                        <td><strong>Offer</strong></td>
                        <td><strong>Validity</strong></td>
                    </tr>
                    <tr style="height: 20px;">
                        <td>Above 45 secs to 60 secs</td>
                        <td>above 999</td>
                        <td>22nd Sep 2019</td>
                    </tr>
                    <tr style="height: 20px;">
                        <td>Between 30 secs to 45 sec</td>
                        <td>Rs. 250 off on shopping above 1499</td>
                        <td>22nd Sep 2019</td>
                    </tr>
                    <tr style="height: 20px;">
                        <td>Under 30 Seconds&nbsp;</td>
                        <td>Rs. 500 off on shopping above 1999</td>
                        <td>22nd Sep 2019</td>
                    </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="js/app.js"></script>
</body>
</html>
