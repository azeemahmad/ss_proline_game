@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            User-Coupon {{ $coupon->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/user') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        {{--<a href="{{ url('/admin/user/' . $user->id . '/edit') }}" title="Edit User">--}}
                        {{--<button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>--}}
                        {{--</a>--}}

                        {{--<form method="POST" action="{{ url('admin/user' . '/' . $user->id) }}" accept-charset="UTF-8"--}}
                        {{--style="display:inline">--}}
                        {{--{{ method_field('DELETE') }}--}}
                        {{--{{ csrf_field() }}--}}
                        {{--<button type="submit" class="btn btn-danger btn-sm" title="Delete User"--}}
                        {{--onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>--}}
                        {{--Delete--}}
                        {{--</button>--}}
                        {{--</form>--}}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>Coupon-code</th>
                                    <td>CouponCode: {{$coupon->couponcode }}</td>
                                </tr>

                                <tr>
                                    <th> Mobile</th>
                                    <td> {{ $user->mobile }} </td>
                                </tr>

                                <tr>
                                    <th> Total Game Moves</th>
                                    <td> {{ $coupon->moves  }} </td>
                                </tr>
                                <tr>
                                    <th> Total Time Taken</th>
                                    <td> {{ $coupon->time }} </td>
                                </tr>
                                <tr>
                                    <th> Game played Date and Time</th>
                                    <td> {{ $user->created_at }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
